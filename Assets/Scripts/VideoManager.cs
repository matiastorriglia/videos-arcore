using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

[System.Serializable]
public class ARVideo
{
    public VideoClip clip;
    public Sprite videoImg;
    public float xOffset, yOffset, zOffset = 0;
}

[System.Serializable]
public class ARVideoList
{
    public ARVideo[] videos;
}

public class VideoManager : MonoBehaviour
{

    public static VideoManager instance = null;

    public ARVideo videoToPlay;

    public ARVideoList[] arVideos;

    public bool firstLoad = false;

    public bool mashup = false;

    // Start is called before the first frame update
    void Awake()
    {
        if (!instance)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            if (this != instance)
                Destroy(gameObject);
        }
    }
}
