/* 
*   NatCorder
*   Copyright (c) 2020 Yusuf Olokoba
*/

    using UnityEngine;
    using System.Collections;
    using UnityEditor;
    using System.IO;
    using NatCorder;
    using NatCorder.Clocks;
    using NatCorder.Inputs;

public class ReplayCam : MonoBehaviour
    {

        public ARSceneManager ar;

        [Header("Recording")]
        private int videoWidth = 1280;
        private int videoHeight = 720;

        public Camera arCamera;
        private IMediaRecorder recorder;
        private CameraInput cameraInput;
        private AudioInput audioInput, audioInput2;
        private AudioSource microphoneSource;
        public AudioSource arAudioSource = null;


        string path;
        byte[] arrayBytes;

        private IEnumerator Start()
        {
            var screenDif = (float)Screen.height / (float)Screen.width;
            //print("ScreenAspect 1/" + screenDif);
            //videoHeight = (int)(screenDif * 500);
            //videoWidth = (int)(500); 
            videoHeight = Screen.height;
            videoWidth = Screen.width;

            //print(videoWidth + " + " + videoHeight);

            // Start microphone
            microphoneSource = gameObject.AddComponent<AudioSource>();
            microphoneSource.mute = microphoneSource.loop = true;
            microphoneSource.bypassEffects = microphoneSource.bypassListenerEffects = false;
            microphoneSource.clip = Microphone.Start(null, true, 10, AudioSettings.outputSampleRate);
            yield return new WaitUntil(() => Microphone.GetPosition(null) > 0);
            microphoneSource.Play();
        }

        private void OnDestroy()
        {
            // Stop microphone
            microphoneSource.Stop();
            Microphone.End(null);
        }

        public void StartRecording()
        {
            //SharingManager.instance.SetUIActive(false);
            //SharingManager.instance.videoButtonStop.SetActive(true);
            //SharingManager.instance.photoImageUXart.SetActive(false);

            // Start recording
            var frameRate = 30;
            var sampleRate = AudioSettings.outputSampleRate;
            var channelCount = (int)AudioSettings.speakerMode;
            var clock = new RealtimeClock();
            recorder = new MP4Recorder(videoWidth, videoHeight, frameRate, sampleRate, channelCount);
            // Create recording inputs
            cameraInput = new CameraInput(recorder, clock, arCamera);
            //audioInput = new AudioInput(recorder, clock, microphoneSource, true);
            if(arAudioSource)
                audioInput2 = new AudioInput(recorder, clock, arAudioSource, false);
            // Unmute microphone
            microphoneSource.mute = audioInput == null;
        }

        public async void StopRecording()
        {
            //SharingManager.instance.SetUIActive(true);
            //SharingManager.instance.Recording = false;

            // Mute microphone
            microphoneSource.mute = true;
            // Stop recording
            audioInput2?.Dispose();
            //audioInput?.Dispose();
            cameraInput?.Dispose();
            path = await recorder.FinishWriting();
            ar.OpenSaveRecording();
            //Handheld.PlayFullScreenMovie(path);
            //finish upload after confirmation

            //confirmCG.interactable = true;
            //confirmCG.blocksRaycasts = true;
            //confirmCG.alpha = 1;
        }

        public string GetPreviewPath()
        {
            return path;
        }

        public void PlayRecording()
        {
            StartCoroutine(UploadWaiter(path));
        }

        private IEnumerator UploadWaiter(string path)
        {
            yield return new WaitForSecondsRealtime(1);
#if UNITY_EDITOR
            EditorUtility.OpenWithDefaultApp(path);
        //new NativeShare().AddFile(path).SetSubject("UXART").SetText("#UXart").Share();
#elif UNITY_IOS
            Handheld.PlayFullScreenMovie("file://" + path);
            //new NativeShare().AddFile("file://" + path).SetSubject("UXART").SetText("#UXart").Share();
#elif UNITY_ANDROID
            Handheld.PlayFullScreenMovie(path);
            //new NativeShare().AddFile(path).SetSubject("UXART").SetText("#UXart").Share();
#endif
    }
}