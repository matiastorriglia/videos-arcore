﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class Billboard : MonoBehaviour
{

    //Camera arCam;
    public VideoPlayer videoPlayer;
    public VideoPlayer shadowPlayer;
    public AudioSource audioSource;
    public MeshRenderer videoRenderer, shadowRenderer;
    public GameObject floorCircle;

    // Start is called before the first frame update
    void Start()
    {
        //arCam = GameObject.Find("AR Camera").GetComponent<Camera>();
        videoPlayer.SetTargetAudioSource(0, audioSource);
        videoPlayer.EnableAudioTrack(0, false);
        videoRenderer.transform.localPosition = new Vector3(VideoManager.instance.videoToPlay.xOffset, 
                                                            VideoManager.instance.videoToPlay.yOffset,
                                                            VideoManager.instance.videoToPlay.zOffset);

        if(SceneManager.GetActiveScene().name == "Selfie" || SceneManager.GetActiveScene().name == "Mashup Selfie")
        {
            floorCircle.SetActive(false);
        }
    }

    public void Hide()
    {
        videoRenderer.enabled = shadowRenderer.enabled = false;
        StartCoroutine(HideCR());
    }

    IEnumerator HideCR()
    {
        videoPlayer.Prepare();
        shadowPlayer.Prepare();
        yield return new WaitWhile(() => !videoPlayer.isPrepared);
        videoPlayer.Play();
        shadowPlayer.Play();
        //videoRenderer.enabled = shadowRenderer.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Mathf.Ceil((float)videoPlayer.time) < 1f)
        {
            videoRenderer.enabled = shadowRenderer.enabled = false;
        }
        else
        {
            videoRenderer.enabled = shadowRenderer.enabled = true;
        }
        //if (arCam)
        //{
        //    Vector3 previousEulerAngles = transform.eulerAngles;
        //    transform.LookAt(arCam.transform);
        //    transform.eulerAngles = new Vector3(previousEulerAngles.x, transform.eulerAngles.y, previousEulerAngles.z);
        //}
    }


}
