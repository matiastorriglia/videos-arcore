using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoButton : MonoBehaviour
{
    // Start is called before the first frame update

    public ARVideo myVideo;
    public Image buttonImage;

    Home homeManager;

    public void InitButton(ARVideo v, Home h) {
        myVideo = v;
        buttonImage.sprite = myVideo.videoImg;
        homeManager = h;
    }

    public void PlayVideo()
    {
        VideoManager.instance.videoToPlay = myVideo;
        homeManager.GoToARSCene();
    }
}
