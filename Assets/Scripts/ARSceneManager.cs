using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Recorder;
using UnityEngine.UI;
using NatCorder.Examples;
using UnityEngine.SceneManagement;
using UnityEngine.Video;
using System.IO;
using UnityEngine.Events;
#if UNITY_ANDROID || UNITY_IOS
using NativeGalleryNamespace;
#endif

public class ARSceneManager : MonoBehaviour
{

    public RecordManager recordManager;
    public ReplayCam replayCam;

    public bool recording = false;

    public Image recordButtonImage;

    public ARTapToPlaceObject arManager;

    public ARTapToPlaceObjectSelfie arSelfieManager;

    public GameObject finishRecordingUI, deleteConfirmUI, exitConfirmUI, downloadConfirmUI, noPermissionUI;
    public VideoPlayer recordingPreviewVideo;

    public RawImage photoPreview;
    string filePath;

    Texture2D ss;

    string galleryPath = "/../../../../DCIM/VideoRecorders";

    public GameObject switchSceneButton;

    public GameObject uiCanvas;

    private void Start()
    {
        NativeGallery.RequestPermission(NativeGallery.PermissionType.Write);

        switchSceneButton.SetActive(!VideoManager.instance.mashup);
    }

    public void RecordButtonPress()
    {

        if (arSelfieManager) {
            TakeScreenshot();
        }else{
            replayCam.arAudioSource = arManager.arAudio;

            if (!recording) {
                replayCam.StartRecording();
                recordButtonImage.color = new Color(1, 0, 0, 0.5f);
                recording = true;
            }
            else
            {
                if (arManager)
                    arManager.ForceStop();

                recordButtonImage.color = new Color(1, 1, 1, 0.5f);
                replayCam.StopRecording();
                recording = false;
            }
        }
    }

    public void StopRecording()
    {
        recordButtonImage.color = new Color(1, 1, 1, 0.5f);
        replayCam.StopRecording();
        recording = false;
    }

    public void OpenSaveRecording()
    {
        finishRecordingUI.SetActive(true);
    }

    public void SaveRecording()
    {
        System.DateTime now = System.DateTime.Now;
        string fileName = "Video_" + now.Year + "_" + now.Month + "_" + now.Day + "_" + now.Hour + "_" + now.Minute + "_" + now.Second + ".mp4";
        NativeGallery.SaveVideoToGallery(File.ReadAllBytes(replayCam.GetPreviewPath()), "Videos AR", fileName);
        File.Delete(replayCam.GetPreviewPath());
        RestartScene();
    }

    public void DeleteRecording()
    {
        File.Delete(replayCam.GetPreviewPath());
    }

    public void ShareRecording()
    {
        new NativeShare().AddFile(replayCam.GetPreviewPath()).Share();
    }

    public void CloseRecording()
    {
        finishRecordingUI.SetActive(false);
    }

    public void GoBack()
    {
        SceneManager.LoadScene("Home");
        Destroy(gameObject);
    }

    public void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void StartARScene()
    {
        SceneManager.LoadScene("AR Scene Main");
    }

    public void StartSelfieScene()
    {
        SceneManager.LoadScene("Selfie");
    }

    public void TakeScreenshot()
    {
        StartCoroutine(TakeSSCR());
    }

    public void SaveScreenshot()
    {
        string fileName = "SharedImage_" + System.DateTime.Now.Year + "-" + System.DateTime.Now.Month + "-" + System.DateTime.Now.Day + "_" +
                                            System.DateTime.Now.Hour + System.DateTime.Now.Minute + System.DateTime.Now.Second + ".jpg";
        NativeGallery.SaveImageToGallery(ss.EncodeToJPG(), "AR Selfies", fileName);
        RestartScene();
    }

    IEnumerator TakeSSCR()
    {
        //videoButtonStop.SetActive(false);
        //photoImageUXart.SetActive(true);
        uiCanvas.SetActive(false);
        yield return new WaitForEndOfFrame();

        ss = new Texture2D(Screen.width, Screen.height, TextureFormat.ARGB32, false);
        ss.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        ss.Apply();

        photoPreview.texture = ss;

        uiCanvas.SetActive(true);
        //filePath = Application.persistentDataPath + "/sharedimage_" + System.DateTime.Now.Year + "-" + System.DateTime.Now.Month + "-" + System.DateTime.Now.Day + "_" + 
        //                                            System.DateTime.Now.Hour + System.DateTime.Now.Minute + System.DateTime.Now.Second + ".png";

        //byte[] jpgBytes = ss.EncodeToJPG();
        //File.WriteAllBytes(filePath, jpgBytes);
        Debug.Log(filePath);

        if (arSelfieManager)
            arSelfieManager.ForceStop();

        finishRecordingUI.SetActive(true);
    }

    public void ShareScreenshot()
    {
        filePath = Application.persistentDataPath + "/sharedimage_" + System.DateTime.Now.Year + "-" + System.DateTime.Now.Month + "-" + System.DateTime.Now.Day + "_" +
                                    System.DateTime.Now.Hour + System.DateTime.Now.Minute + System.DateTime.Now.Second + ".png";

        byte[] jpgBytes = ss.EncodeToJPG();
        File.WriteAllBytes(filePath, jpgBytes);
        new NativeShare().AddFile(filePath).Share();
        File.Delete(replayCam.GetPreviewPath());
    }
}
