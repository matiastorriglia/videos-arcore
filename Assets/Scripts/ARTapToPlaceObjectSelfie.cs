﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.SceneManagement;
using UnityEngine.Video;
public class ARTapToPlaceObjectSelfie : MonoBehaviour
{

    public GameObject gameObjectToInstantiate;

    GameObject spawnedObj = null;
    GameObject objectIndicator = null;
    //public ARRaycastManager arRay;

    //public ARPlaneManager arPlane;

    public ARCameraManager camManager;
    public ARSceneManager arScene;
    public ARSession arSession;

    public VideoPlayer videoPlayer;

    public AudioSource arAudio;

    Vector2 touchPos;

    static List<ARRaycastHit> hits = new List<ARRaycastHit>();

    public GameObject recordButton, spawnButton;
    public GameObject indicatorPrefab;

    float spawnCounter = 0;
    float spawnTime = 3.5f;

    public Animator panelAnim;
    public bool mashup = false;

    private void Start()
    {
        //camManager.requestedFacingDirection = CameraFacingDirection.User;
    }


    bool TryGetTouchPosition(out Vector2 touchPos)
    {
        if(Input.touchCount > 0)
        {
            touchPos = Input.GetTouch(0).position;
            return true;
        }

        touchPos = default;
        return false;
    }

    // Update is called once per frame
    void Update()
    {

        if (objectIndicator == null && spawnedObj == null)
        {
            //if (!TryGetTouchPosition(out Vector2 touchPos))
            //{
            //    return;
            //}

            if(spawnCounter < spawnTime)
            {
                spawnCounter += Time.deltaTime;
                return;
            }

            //if (Physics.Raycast(touchPos,camManager.transform.forward, out RaycastHit hit))
            //{

            //}
            panelAnim.Play("messagefade", -1, 0);
            objectIndicator = Instantiate(indicatorPrefab, camManager.transform.position + camManager.transform.forward * 2f, Quaternion.identity);
            objectIndicator.transform.localScale = new Vector3(0.65f, 0.65f, 0.65f);
            objectIndicator.AddComponent<Lean.Touch.LeanDragTranslate>();
            objectIndicator.AddComponent<Lean.Touch.LeanPinchScale>();
            Vector3 previousEulerAngles = objectIndicator.transform.eulerAngles;
            objectIndicator.transform.LookAt(camManager.transform);
            objectIndicator.transform.eulerAngles = new Vector3(previousEulerAngles.x, objectIndicator.transform.eulerAngles.y, previousEulerAngles.z);

            if (!mashup)
                spawnButton.SetActive(true);
        }
        else if(objectIndicator != null && spawnedObj == null)
        {

            if (TryGetTouchPosition(out Vector2 touchPos))
            {
                Vector3 previousEulerAngles = objectIndicator.transform.eulerAngles;
                objectIndicator.transform.LookAt(camManager.transform);
                objectIndicator.transform.eulerAngles = new Vector3(previousEulerAngles.x, objectIndicator.transform.eulerAngles.y, previousEulerAngles.z);
            }
        }
        else if(objectIndicator == null && spawnedObj != null)
        {
            if (TryGetTouchPosition(out Vector2 touchPos))
            {
                Vector3 previousEulerAngles = spawnedObj.transform.eulerAngles;
                spawnedObj.transform.LookAt(camManager.transform);
                spawnedObj.transform.eulerAngles = new Vector3(previousEulerAngles.x, spawnedObj.transform.eulerAngles.y, previousEulerAngles.z);
            }

            CheckOver();
        }    
    }

    public void SpawnObject()
    {
        if (objectIndicator)
        {
            spawnButton.SetActive(false);
            spawnedObj = Instantiate(gameObjectToInstantiate, objectIndicator.transform.position, objectIndicator.transform.rotation);
            spawnedObj.transform.localScale = objectIndicator.transform.localScale;

            Billboard spawnedManager = spawnedObj.GetComponent<Billboard>();
            spawnedManager.videoRenderer.enabled = spawnedManager.shadowRenderer.enabled = false;
            arAudio = spawnedManager.audioSource;
            videoPlayer = spawnedManager.videoPlayer;
            videoPlayer.clip = VideoManager.instance.videoToPlay.clip;
            spawnedManager.Hide();

            Destroy(objectIndicator);
            objectIndicator = null;
            spawnedObj.AddComponent<Lean.Touch.LeanDragTranslate>();
            spawnedObj.AddComponent<Lean.Touch.LeanPinchScale>();

            Vector3 previousEulerAngles = spawnedObj.transform.eulerAngles;
            spawnedObj.transform.LookAt(camManager.transform);
            spawnedObj.transform.eulerAngles = new Vector3(previousEulerAngles.x, spawnedObj.transform.eulerAngles.y, previousEulerAngles.z);
            recordButton.SetActive(true);
        }
    }

    public void ForceStop()
    {
        arAudio = null;
        Destroy(spawnedObj);
        spawnedObj = null;
        videoPlayer = null;
        spawnButton.SetActive(true);
    }

    private void CheckOver()
    {
        if (videoPlayer) {
            if (Mathf.Ceil((float)videoPlayer.time) >= Mathf.Ceil((float)videoPlayer.length))
            {
                if (arScene.recording)
                    arScene.StopRecording();
                recordButton.SetActive(false);

                objectIndicator = Instantiate(indicatorPrefab, spawnedObj.transform.position, spawnedObj.transform.rotation);
                objectIndicator.transform.localScale = spawnedObj.transform.localScale;
                objectIndicator.AddComponent<Lean.Touch.LeanDragTranslate>();
                objectIndicator.AddComponent<Lean.Touch.LeanPinchScale>();

                arAudio = null;
                Destroy(spawnedObj);
                spawnedObj = null;
                videoPlayer = null;
                spawnButton.SetActive(true);
            }
        }
    }
}
