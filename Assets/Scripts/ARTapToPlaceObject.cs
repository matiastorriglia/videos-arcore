﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.SceneManagement;
using UnityEngine.Video;
public class ARTapToPlaceObject : MonoBehaviour
{

    public GameObject gameObjectToInstantiate;

    GameObject spawnedObj = null;
    GameObject objectIndicator = null;
    public ARRaycastManager arRay;

    public ARPlaneManager arPlane;

    public Camera cam;
    public ARCameraManager camManager;
    public ARSceneManager arScene;
    public ARSession arSession;

    public VideoPlayer videoPlayer;

    public AudioSource arAudio;

    Vector2 touchPos;

    static List<ARRaycastHit> hits = new List<ARRaycastHit>();

    public GameObject recordButton, spawnButton;
    public GameObject indicatorPrefab;
    public Animator panelAnim;

    public bool mashup = false;

    private void Start()
    {
        //camManager.requestedFacingDirection = CameraFacingDirection.User;
    }

    public void ChangeCamera()
    {
        //StartCoroutine(ChangeCameraCR());
    }

    public void SelfieCameraScene()
    {
        SceneManager.LoadScene("Selfie");
    }

    public void FrontCameraScene()
    {
        SceneManager.LoadScene("FrontCamera");
    }

    IEnumerator ChangeCameraCR()
    {

        arPlane.enabled = false;
        arSession.enabled = false;

        yield return new WaitForEndOfFrame();

        Destroy(spawnedObj.gameObject);
        spawnedObj = null;

        if (camManager.requestedFacingDirection == CameraFacingDirection.World)
            camManager.requestedFacingDirection = CameraFacingDirection.User;
        else
            camManager.requestedFacingDirection = CameraFacingDirection.World;

        yield return new WaitForEndOfFrame();

        arSession.Reset();
        arSession.enabled = true;
        arPlane.enabled = true;
    }

    private void PlaneChanged(ARPlanesChangedEventArgs args)
    {
        if (args.added != null && spawnedObj == null)
        {
            ARPlane firstPlane = args.added[0];
            spawnedObj = Instantiate(gameObjectToInstantiate, firstPlane.transform.position, Quaternion.identity);
            Vector3 previousEulerAngles = spawnedObj.transform.eulerAngles;
            spawnedObj.transform.LookAt(camManager.transform);
            spawnedObj.transform.eulerAngles = new Vector3(previousEulerAngles.x, spawnedObj.transform.eulerAngles.y, previousEulerAngles.z);
        }
    }

    bool TryGetTouchPosition(out Vector2 touchPos)
    {
        if(Input.touchCount > 0)
        {
            touchPos = Input.GetTouch(0).position;
            return true;
        }

        touchPos = default;
        return false;
    }

    // Update is called once per frame
    void Update()
    {

        //if (spawnedObj == null)
        //{
        //    arPlane.planesChanged += PlaneChanged;
        //}
        //else {

        //    if (!TryGetTouchPosition(out Vector2 touchPos))
        //    {
        //        return;
        //    }

        //    if(arRay.Raycast(touchPos, hits, TrackableType.PlaneWithinPolygon))
        //    {
        //        var hitPose = hits[0].pose;
        //        spawnedObj.transform.position = hitPose.position;
        //        Vector3 previousEulerAngles = spawnedObj.transform.eulerAngles;
        //        spawnedObj.transform.LookAt(camManager.transform);
        //        spawnedObj.transform.eulerAngles = new Vector3(previousEulerAngles.x, spawnedObj.transform.eulerAngles.y, previousEulerAngles.z);
        //    }
        //}

        if (objectIndicator == null && spawnedObj == null)
        {
            //if (!TryGetTouchPosition(out Vector2 touchPos))
            //{
            //    return;
            //}

            //(arRay.Raycast(touchPos, hits, TrackableType.PlaneWithinPolygon))
            if (arRay.Raycast(cam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0f)), hits, TrackableType.PlaneWithinPolygon))
            {
                panelAnim.Play("messagefade", -1, 0);
                objectIndicator = Instantiate(indicatorPrefab, hits[0].pose.position, Quaternion.identity);
                objectIndicator.AddComponent<Lean.Touch.LeanDragTranslate>();
                objectIndicator.AddComponent<Lean.Touch.LeanPinchScale>();
                Vector3 previousEulerAngles = objectIndicator.transform.eulerAngles;
                objectIndicator.transform.LookAt(camManager.transform);
                objectIndicator.transform.eulerAngles = new Vector3(previousEulerAngles.x, objectIndicator.transform.eulerAngles.y, previousEulerAngles.z);
                spawnButton.SetActive(true);
            }
        }
        else if(objectIndicator != null && spawnedObj == null)
        {

            if (TryGetTouchPosition(out Vector2 touchPos))
            {
                Vector3 previousEulerAngles = objectIndicator.transform.eulerAngles;
                objectIndicator.transform.LookAt(camManager.transform);
                objectIndicator.transform.eulerAngles = new Vector3(previousEulerAngles.x, objectIndicator.transform.eulerAngles.y, previousEulerAngles.z);
            }
        }
        else if(objectIndicator == null && spawnedObj != null)
        {
            if (TryGetTouchPosition(out Vector2 touchPos))
            {
                Vector3 previousEulerAngles = spawnedObj.transform.eulerAngles;
                spawnedObj.transform.LookAt(camManager.transform);
                spawnedObj.transform.eulerAngles = new Vector3(previousEulerAngles.x, spawnedObj.transform.eulerAngles.y, previousEulerAngles.z);
            }

            CheckOver();
        }    
    }

    public void SpawnObject()
    {
        if (objectIndicator)
        {
            spawnButton.SetActive(false);
            spawnedObj = Instantiate(gameObjectToInstantiate, objectIndicator.transform.position, objectIndicator.transform.rotation);
            spawnedObj.transform.localScale = objectIndicator.transform.localScale;

            Destroy(objectIndicator);
            objectIndicator = null;
            spawnedObj.AddComponent<Lean.Touch.LeanDragTranslate>();
            spawnedObj.AddComponent<Lean.Touch.LeanPinchScale>();

            Billboard spawnedManager = spawnedObj.GetComponent<Billboard>();
            arAudio = spawnedManager.audioSource;
            videoPlayer = spawnedManager.videoPlayer;
            videoPlayer.clip = VideoManager.instance.videoToPlay.clip;
            spawnedManager.Hide();

            Vector3 previousEulerAngles = spawnedObj.transform.eulerAngles;
            spawnedObj.transform.LookAt(camManager.transform);
            spawnedObj.transform.eulerAngles = new Vector3(previousEulerAngles.x, spawnedObj.transform.eulerAngles.y, previousEulerAngles.z);

            recordButton.SetActive(!VideoManager.instance.mashup);
        }
    }

    public void ForceStop()
    {
        arAudio = null;
        Destroy(spawnedObj);
        spawnedObj = null;
        videoPlayer = null;
        spawnButton.SetActive(true);
    }

    private void CheckOver()
    {

        if (videoPlayer) { 
            if (Mathf.Ceil((float)videoPlayer.time) >= Mathf.Ceil((float)videoPlayer.length))
            {
                if(arScene.recording)
                    arScene.StopRecording();
                recordButton.SetActive(false);

                objectIndicator = Instantiate(indicatorPrefab, spawnedObj.transform.position, spawnedObj.transform.rotation);
                objectIndicator.transform.localScale = spawnedObj.transform.localScale;
                objectIndicator.AddComponent<Lean.Touch.LeanDragTranslate>();
                objectIndicator.AddComponent<Lean.Touch.LeanPinchScale>();

                arAudio = null;
                Destroy(spawnedObj);
                spawnedObj = null;
                videoPlayer = null;
                spawnButton.SetActive(true);
            }
        }
    }
}
