using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Splash : MonoBehaviour
{

    float splashCounter = 0;
    float splashTime = 2.5f;

    public Animator splashAnim;
    public Button splashButton;
    public Home homeManager;

    private void Awake()
    {
        if (VideoManager.instance.firstLoad)
        {
            gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(splashCounter < splashTime)
        {
            splashCounter += Time.deltaTime;
        }
        else
        {
            Home();
        }
    }

    public void Home()
    {
        if(PlayerPrefs.GetInt("FirstOnboarding", 0) == 0)
        {
            homeManager.OpenOnboarding();
        }

        this.enabled = false;
        splashButton.enabled = false;
        splashAnim.Play("messagefade", -1, 0);
        VideoManager.instance.firstLoad = true;
    }
}
