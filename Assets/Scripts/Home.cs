using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;
using UnityEngine.UI;

[System.Serializable]
public enum HomeOptions
{
    None,
    SingAndDance,
    Challenge,
    Mashup,
    Selfie
}

public class Home : MonoBehaviour
{

    public CanvasGroup onboardingCG;
    public Animator sideMenuAnim, menuAnim, onboardingAnim;

    public GameObject[] menus;

    public Image menuVideoImg;

    public Transform videoButtonParent;
    public GameObject videoButtonPrefab, comingSoonPrefab;

    public GameObject mashupVideoParent;
    public VideoPlayer mashupVideo;

    HomeOptions current;

    Touch touch;
    Vector2 initialPosition;

    int onboardingCurrent = 1;

    bool onboardingOpen = false;
    bool allowSwipe = false;

    void Update()
    {
        if(onboardingOpen && allowSwipe)
            CheckSwipe();
    }

    void CheckSwipe()
    {
        if (Input.touchCount == 1)
        {
            touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                initialPosition = touch.position;
            }
            else if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary)
            {
                // get the moved direction compared to the initial touch position
                var direction = touch.position - initialPosition;

                if (direction.x < -1f)
                {
                    initialPosition = touch.position;
                    if (onboardingCurrent != 4)
                        OnboardingNext();
                }
                else if (direction.x > 1f)
                {
                    initialPosition = touch.position;
                    if (onboardingCurrent != 1)
                        OnboardingPrevious();
                }
            }else if(touch.phase == TouchPhase.Ended)
            {
                initialPosition = touch.position;
            }
        }
    }

    public void OpenMenu(int h)
    {
        for (int i = 0; i < menus.Length; i++)
        {
            menus[i].SetActive(false);
        }

        current = (HomeOptions)h;
        menus[h - 1].SetActive(true);

        for (int i = videoButtonParent.childCount - 1; i >= 0; i--)
        {
            Destroy(videoButtonParent.GetChild(i).gameObject);
        }

        for (int i = 0; i < VideoManager.instance.arVideos[h - 1].videos.Length; i++)
        {
            VideoButton newVid = Instantiate(videoButtonPrefab, videoButtonParent).GetComponent<VideoButton>();
            newVid.InitButton(VideoManager.instance.arVideos[h - 1].videos[i], this);
        }

        Instantiate(comingSoonPrefab, videoButtonParent);
        Instantiate(comingSoonPrefab, videoButtonParent);

        menuAnim.Play("Open", -1, 0);
    }

    public void CloseMenu()
    {
        menuAnim.Play("Close", -1, 0);
    }

    public void OpenMashupVideo()
    {
        mashupVideo.clip = VideoManager.instance.videoToPlay.clip;
        mashupVideoParent.SetActive(true);
        mashupVideo.Play();
    }

    public void CloseMashupVideo()
    {
        mashupVideoParent.SetActive(false);
    }

    public void OpenSideMenu()
    {
        sideMenuAnim.Play("Open", -1, 0);
    }

    public void CloseSideMenu()
    {
        sideMenuAnim.Play("Close", -1, 0);
    }

    public void OpenOnboarding()
    {
        onboardingOpen = true;
        onboardingAnim.Play("OpenOnboarding", -1, 0);
        StartCoroutine(LockSwipe());
    }

    public void QuitApp()
    {
        Application.Quit();
    }

    public void GoToARSCene()
    {
        switch (current)
        {
            case HomeOptions.SingAndDance:
                VideoManager.instance.mashup = false;
                SceneManager.LoadScene("AR Scene Main");
                break;

            case HomeOptions.Challenge:
                VideoManager.instance.mashup = false;
                SceneManager.LoadScene("AR Scene Main");
                break;

            case HomeOptions.Selfie:
                VideoManager.instance.mashup = false;
                SceneManager.LoadScene("Selfie");
                break;

            case HomeOptions.Mashup:
                VideoManager.instance.mashup = true;
                SceneManager.LoadScene("AR Scene Main");
                break;
        }


    }

    public void GoToSelfieSCene()
    {
        SceneManager.LoadScene("Selfie");
    }

    public void OnboardingNext()
    {
        StartCoroutine(LockSwipe());

        switch (onboardingCurrent)
        {
            case 1:
                onboardingCurrent++;
                onboardingAnim.Play("OBSwipe12", -1, 0);
            break;

            case 2:
                onboardingCurrent++;
                onboardingAnim.Play("OBSwipe23", -1, 0);
                break;

            case 3:
                onboardingCurrent++;
                onboardingAnim.Play("OBSwipe34", -1, 0);
                break;

            case 4:
                onboardingCurrent = 1;
                CloseOnboarding();
                break;
        }
    }

    public void OnboardingPrevious()
    {
        StartCoroutine(LockSwipe());

        switch (onboardingCurrent)
        {
            case 1:
                CloseOnboarding();
                break;

            case 2:
                onboardingCurrent--;
                onboardingAnim.Play("OBSwipe21", -1, 0);
                break;

            case 3:
                onboardingCurrent--;
                onboardingAnim.Play("OBSwipe32", -1, 0);
                break;

            case 4:
                onboardingCurrent--;
                onboardingAnim.Play("OBSwipe43", -1, 0);
                break;
        }
    }

    IEnumerator LockSwipe()
    {
        allowSwipe = false;
        yield return new WaitForSeconds(0.65f);
        allowSwipe = true;
    }

    public void CloseOnboarding()
    {
        onboardingOpen = false;
        onboardingAnim.Play("New State", -1, 0);
        onboardingCG.alpha = 0;
        onboardingCG.interactable = onboardingCG.blocksRaycasts = false;
        PlayerPrefs.SetInt("FirstOnboarding", 1);
    }
}
